package com.example.anita.testcommunity1.model;

//import java.util.ArrayList;

/**
 * Created by do_menico on 07/09/2017.
 */

public class Community {

    private String name;
    private String id;
    private String data;
//    private ArrayList<Topic> topics;

    public Community(String name, String id, String data) {
        this.name = name;
        this.id = id;
        this.data = data;
    }

    public Community() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

//    public Community(String name, ArrayList<Topic> topics) {
//        this.name = name;
//        this.topics = topics;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public ArrayList<Topic> getTopics() {
//        return topics;
//    }
//
//    public void setTopics(ArrayList<Topic> topics) {
//        this.topics = topics;
//    }
}
