package com.example.anita.testcommunity1.modelTemp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by do_menico on 07/09/2017.
 */

public class User {

    private String username;
    private String password;
    private List<Community> communities;

    public User() {
    }

    public User(String username, String password, List<Community> communities) {
        this.username = username;
        this.password = password;
        this.communities = communities;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Community> getCommunities() {
        return communities;
    }

    public void setCommunities(List<Community> communities) {
        this.communities = communities;
    }
}
