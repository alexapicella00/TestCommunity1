package com.example.anita.testcommunity1.helper;

import com.example.anita.testcommunity1.modelTemp.Topic;

import java.util.ArrayList;

public class TopicsList {

    public static ArrayList<Topic> getTopicCommunity(String com){
        ArrayList<Topic> list = new ArrayList<>();
        String message = com + ": TEST MESSAGE";
        for (int i = 0; i<10; ++i)
            list.add(new Topic(com, "Nome " + i, "2017,11,22","android.com", message));
        return list;
    }

}
