package com.example.anita.testcommunity1.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.anita.testcommunity1.R;
import com.example.anita.testcommunity1.modelTemp.Topic;

import java.util.ArrayList;

public class TopicsAdapter extends ArrayAdapter<Topic> {

    public TopicsAdapter(Context context, ArrayList<Topic> topics) {
        super(context, 0, topics);
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

         if ( convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_topic, null);
        }

        Topic topic = (Topic) getItem(i);
        TextView tvTitle = (TextView) convertView.findViewById(R.id.topicTitle);
        TextView tvAuthor = (TextView) convertView.findViewById(R.id.topicAuthor);
        TextView tvDate = (TextView) convertView.findViewById(R.id.topicDate);
        TextView tvLink = (TextView) convertView.findViewById(R.id.topicLink);
        tvTitle.setText(topic.getTitle());
        tvAuthor.setText(topic.getAuthor());
        tvDate.setText(topic.getDate());
        tvLink.setText(topic.getLink());
        return convertView;
    }
}
