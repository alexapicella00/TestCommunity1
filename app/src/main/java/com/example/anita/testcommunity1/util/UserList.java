package com.example.anita.testcommunity1.util;

import com.example.anita.testcommunity1.modelTemp.Community;
import com.example.anita.testcommunity1.modelTemp.Topic;
import com.example.anita.testcommunity1.modelTemp.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by do_menico on 08/09/2017.
 */

public class UserList {

    public static List<User> getUsers(){
       List<User> list = new ArrayList<>();
        for (int i = 0; i<10; i++)
            list.add(new User("User " + i, "00" + i, getCommunities() ));
        return list;
    }

    public static List<Community> getCommunities() {
        List<Community> list = new ArrayList<>();
        for (int i = 0; i<10; i++)
            list.add(new Community("Community " + i, "0" + i, "2017,11,22", getTopics()));
        return list;
    }

    public static List<Topic> getTopics() {
        List<Topic> list = new ArrayList<>();
        String message =  ": TEST MESSAGE";
        for (int i = 0; i<10; ++i)
            list.add(new Topic("Nome " + i, "2017,11,22","android.com", message));
        return list;
    }
}
