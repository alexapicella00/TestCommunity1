package com.example.anita.testcommunity1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.anita.testcommunity1.helper.TopicsList;
import com.example.anita.testcommunity1.modelTemp.Topic;
import com.example.anita.testcommunity1.model.TopicsAdapter;


public class CommunityActivity extends AppCompatActivity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.listView1);

        populateView(listView, getIntent().getExtras().getString("community"));

    }

    public void goOnTopic(Topic topic) {
        Intent intent = new Intent(this, TopicViewer.class);
        intent.putExtra("topic", topic);
        startActivity(intent);
    }

    public void populateView(final ListView listView, String com){
        listView.setAdapter(new TopicsAdapter(this, TopicsList.getTopicCommunity(com)));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Topic t = (Topic) listView.getAdapter().getItem(position);
                goOnTopic(t);
            }
        });
    }

}
