package com.example.anita.testcommunity1;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.*;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.anita.testcommunity1.model.Community;
import com.example.anita.testcommunity1.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    private FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getAllCommunity(database, "https://testloadcommunity.firebaseio.com/ALLUSERS/"
                + getIntent().getExtras().getString("refer") + "/community");
    }

    public void getAllCommunity(FirebaseDatabase database, String userRef){

        DatabaseReference userReference = database.getReferenceFromUrl(userRef);
        userReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                populate(dataSnapshot, (LinearLayout) findViewById(R.id.layout));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void populate(DataSnapshot dataSnapshot, LinearLayout layout){
        layout.removeAllViews();
        for (DataSnapshot s : dataSnapshot.getChildren()){
            Log.i("verifica", dataSnapshot.getChildren().toString());
            layout.addView(createButton(s.getValue(Community.class).getName()));
        }
    }

    public Button createButton(String text){
        Button myBut = new Button(this);
        myBut.setText(text);
        myBut.setHeight(60);
        myBut.setBackgroundColor(50);

        myBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goOnCommunity( ((Button) v).getText().toString() );
            }
        });
        return myBut;
    }

    public void goOnCommunity(String community) {
        Intent intent = new Intent(this, CommunityActivity.class);
        intent.putExtra("community", community);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}