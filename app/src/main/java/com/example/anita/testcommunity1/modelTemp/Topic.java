package com.example.anita.testcommunity1.modelTemp;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Topic implements Serializable {

    private String title;
    private String author;
    private String date;
    private String message;

    public Topic() {
    }

    public Topic(String title, String author, String date, String message) {
        this.title = title;
        this.author = author;
        this.date = date;
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
