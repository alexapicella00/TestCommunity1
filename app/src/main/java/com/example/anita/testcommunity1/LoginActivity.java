package com.example.anita.testcommunity1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.anita.testcommunity1.model.User;
import com.example.anita.testcommunity1.model.UserWrapper;
import com.example.anita.testcommunity1.util.InternetConnection;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private final int NO_INTERNET = 0;
    private final int WRONG_USER = 1;
    private final int WRONG_PASS = 2;
    private final int MISS_USER = 3;
    private final int MISS_PASS = 4;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private Map<String, UserWrapper> utenti = new HashMap();

    private EditText userTxt;
    private EditText passTxt;
    private Button loginBtt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        showToast(-1);

        if (!InternetConnection.haveInternetConnection(this)){
            noInternet();
        } else {
            yesInternet();
        }

    }

    public void noInternet(){
        showToast(NO_INTERNET);
    }

    public void yesInternet(){
        DatabaseReference reference = database.getReference("ALLUSERS");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot s : dataSnapshot.getChildren()){
                    UserWrapper u = new UserWrapper(s.getValue(User.class), s.getKey());
                    utenti.put(u.getUser().getUsername(), u);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //TODO ON CANCELLED
            }
        });

        userTxt = (EditText) findViewById(R.id.userTxt);
        passTxt = (EditText) findViewById(R.id.passTxt);
        loginBtt = (Button) findViewById(R.id.loginBtt);

        loginBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userTxt.getText().toString().length()>0) {
                    if (passTxt.getText().toString().length()>0) {
                        UserWrapper user = utenti.get(userTxt.getText().toString());
                        if (user != null) {
                            if (user.getUser().getPassword().equals(passTxt.getText().toString())) {
                                goOnMain(user.getUser(), user.getRefer());
                            } else {
                                showToast(WRONG_PASS);
                            }
                        } else {
                            showToast(WRONG_USER);
                        }
                    } else {
                        showToast(MISS_PASS);
                    }
                } else {
                    showToast(MISS_USER);
                }
            }
        });
    }

    public void goOnMain(User user, String refer) {
        Intent intent = new Intent(this, MainActivity.class);
        Log.i("MEX", refer);
        intent.putExtra("refer", refer);
        intent.putExtra("user", user);
        startActivity(intent);
    }

    public void showToast(int idT){
        Toast toast = Toast.makeText(this, "BENVENUTI IN MY COMMUNITY!", Toast.LENGTH_LONG);
        switch (idT){
            case 0: toast = Toast.makeText(this, "CONNESSIONE INTERNET ASSENTE!", Toast.LENGTH_LONG);
                break;
            case 1: toast = Toast.makeText(this, "USERNAME NON PRESENTE!", Toast.LENGTH_LONG);
                break;
            case 2: toast = Toast.makeText(this, "PASSWORD ERRATA!", Toast.LENGTH_LONG);
                break;
            case 3: toast = Toast.makeText(this, "INSERISCI UTENTE!", Toast.LENGTH_LONG);
                break;
            case 4: toast = Toast.makeText(this, "TEST TOAST!", Toast.LENGTH_LONG);
                break;
        }
        toast.show();
    }

}

