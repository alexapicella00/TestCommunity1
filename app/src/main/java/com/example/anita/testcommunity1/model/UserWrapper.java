package com.example.anita.testcommunity1.model;

public class UserWrapper{

    private User user;
    private String refer;

    public UserWrapper() {
    }

    public UserWrapper(User user, String refer) {
        this.user = user;
        this.refer = refer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }
}
