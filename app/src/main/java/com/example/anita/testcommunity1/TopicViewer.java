package com.example.anita.testcommunity1;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.anita.testcommunity1.modelTemp.Topic;

public class TopicViewer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_viewer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Topic topic = getIntent().getExtras().getParcelable("topic");


        ((TextView) findViewById(R.id.tvAuthor)).setText(topic.getAuthor());
        ((TextView) findViewById(R.id.tvDate)).setText(topic.getDate());
        ((TextView) findViewById(R.id.tvLink)).setText(topic.getLink());
        ((TextView) findViewById(R.id.tvMessage)).setText(topic.getMessage());
        ((TextView) findViewById(R.id.tvTitle)).setText(topic.getTitle());

    }



}
