package com.example.anita.testcommunity1.modelTemp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by do_menico on 07/09/2017.
 */

public class Community implements Serializable {

    private String name;
    private String id;
    private String date;
    private List<Topic> topics;

    public Community() {
    }

    public Community(String name, String id, String date, List<Topic> topics) {
        this.name = name;
        this.id = id;
        this.date = date;
        this.topics = topics;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }
}
